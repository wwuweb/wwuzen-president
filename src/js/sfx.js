(function ($, Drupal, window, document, undefined) {

  var currentOffset = 0;

  Drupal.behaviors.mainMenuOffset = {

    attach: function (context) {
      var $main_menu = $('#main-menu', context);

      $main_menu.css({
        bottom: -($main_menu.height() / 2),
      });

    }

  };

  Drupal.behaviors.addHoverEffectToPurposeField = {

    attach: function(context) {

      function centerTooltipTextWithTooltip(offset) {
        var halfHeightOfTooltipText = parseInt($('.tooltiptext').height() / 2);
        $('.tooltiptext').css("top", -halfHeightOfTooltipText + parseInt(offset));
      }
      
      var text = $(".purpose-of-event-field .description").text();
      var desiredOutcomesText = "<ul>";
      text = text.split("-");
      for (var i = 1; i < text.length; i++) {
        desiredOutcomesText += "<li>" + text[i] + "</li>";
      }
      desiredOutcomesText += "</ul>";
      
      $('.purpose-of-event-field > label').append("<div class='tooltip'><span>?</span><div class='tooltiptext'>For example:" + desiredOutcomesText + "</div></div>");

      enquire.register("screen and (max-width: 800px)", {
        deferSetup : true,
        match : function() {
          currentOffset = 15;
          centerTooltipTextWithTooltip(currentOffset);
        }
      }).register("screen and (min-width: 801px)", {
        match : function() {
          if (currentOffset === 15) {
            currentOffset = 15;
          }
          else {
            currentOffset = 5;
          }
          centerTooltipTextWithTooltip(currentOffset);
        }
      });
    }
  };

}) (jQuery, Drupal, this, this.document);
